<?php session_start();
   if(isset($_POST['submit'])){
      require 'config.php';
      $insertOneResult = $collection->insertOne([
          'kode_artikel' => $_POST['kode_artikel'],
          'judul_artikel' => $_POST['judul_artikel'],
          'link_artikel' => $_POST['link_artikel'],
          'tanggal_artikel' => $_POST['tanggal_artikel'],
          'isi_artikel' => $_POST['isi_artikel'],
      ]);
      $_SESSION['success'] = "Data Artikel Berhasil di tambahkan";
      header("Location: index.php");
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <title>GAMING Q</title>
      <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
   </head>
   <body>
      <div class="container">
         <br>
         <CENTER><h1>Tambah Data Artikel</h1></CENTER>
         <a href="index.php" class="btn btn-primary">Kembali</a>
         <form method="POST">
            <div class="form-group">
               <strong>Kode Artikel :</strong>
               <input type="text" class="form-control" name="kode_artikel" required="" placeholder="123">
               <strong>Judul :</strong>
               <input type="text" class="form-control" name="judul_artikel" placeholder="Judul Artikel">
               <strong>Link :</strong>
               <input type="text" class="form-control" name="link_artikel" placeholder="https:/xxxx">
               <strong>Tanggal Publish :</strong>
               <input type="text" class="form-control" name="tanggal_artikel" placeholder="Tanggal - Bulan - Tahun">
               <strong>Isi :</strong>
               <input type="text" class="form-control" name="isi_artikel" placeholder="Isi Artikel">
               <br>
               <button type="submit" name="submit" class="btn btn-success">Tambah</button>
            </div>
         </form>
      </div>
   </body>
</html>
